import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import { PropertyService } from 'projects/cloudos/src/app/property/property.service';
import { UserService } from 'projects/cloudos/src/app/user/user.service';

@Component({
  selector: 'shell',
  templateUrl: '../../../cloudos/src/app/shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements AfterViewInit {
  private SEARCH_MODE: string = 'search_mode';
  CONTENT_MODE: string = 'content_mode';
  SHELL_MODE : string = this.CONTENT_MODE;
  isAppReady: boolean = false;
  isLeftNavBar: boolean = false;
  isLoading: boolean = true;
  isShowDPCover: boolean = true;
  headless: boolean = true;
  constructor(private http: HttpClient
    , private router: Router
    , private propertyService: PropertyService
    , private userService: UserService){

  }
  getProperties(){
    return this.http.get('api/1/user/properties?inclTenant=true').toPromise();
  }
  getApplicationContext(){
    return this.http.get('api/1/module/intransit').toPromise();
  }
  getLoggedUserInfo(){
    return this.http.get('api/1/user').toPromise();
  }
  //PROPERTY_TABLE : 'api/1/user/properties?inclTenant=true'
  //APP_CONTEXT : 'api/1/module/indoor'
  // CURRENT_USER :  'api/1/user'
  ngAfterViewInit(): void {
    Promise.all([this.getProperties(), this.getApplicationContext(),this.getLoggedUserInfo()])
    .then((responses) => {
      this.userService.setUser(responses[2]);
      this.propertyService.setProperties(responses[0]);
      this.router.navigate(['/shipment']);
    });
  }
  showAdminNav(){

  }
  closeLeftNav(){

  }
}
