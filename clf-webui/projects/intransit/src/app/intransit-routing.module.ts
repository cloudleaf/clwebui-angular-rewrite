import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShipmentsDashboardComponent } from './shipment/dashboard/shipments-dashboard.component';

const routes: Routes = [
  {
    path: 'shipment',
    component: ShipmentsDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class IntransitRoutingModule { }
