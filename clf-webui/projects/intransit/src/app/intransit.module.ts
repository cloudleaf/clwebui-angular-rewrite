import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IntransitRoutingModule } from './intransit-routing.module';
import { ShellComponent } from './shell.component';
import { ShipmentsDashboardComponent } from './shipment/dashboard/shipments-dashboard.component';
import { UserService } from '../../../cloudos/src/app/user/user.service';
import { PropertyService } from '../../../cloudos/src/app/property/property.service';
import { TopNavComponent } from '../../../cloudos/src/app/layout/top-nav/top-nav.component';


@NgModule({
  declarations: [
    ShellComponent,
    ShipmentsDashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    IntransitRoutingModule
  ],
  providers: [UserService, PropertyService],
  bootstrap: [ShellComponent]
})
export class IntransitModule { }
