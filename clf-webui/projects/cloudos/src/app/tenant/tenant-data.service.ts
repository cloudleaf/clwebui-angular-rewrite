import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as lodash from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class TenantDataService {

  constructor(private httpClient: HttpClient) { }
  getAppRoles() {
    return this.fetchData('api/1/apps');
  }
  getCustomer() {
    return this.fetchData('api/1/customer');
  }
  getCustomerById(id: string) {
    return this.fetchData('api/1/customer/' + id);
  }
  getTenantsMetrics() {
    return this.fetchData('api/1/metrics/all/device/healthcounters');
  }
  fetchData(url: string) {
    return new Promise((resolve, reject)=>{
      this.httpClient.get(url).toPromise()
      .then((response : any) => {
        if(lodash.isEmpty(response.data)){
          resolve(response.data);
        }else{
          reject();
        }
      })
      .catch(()=>{
        reject();
      });
    });
  }
}
