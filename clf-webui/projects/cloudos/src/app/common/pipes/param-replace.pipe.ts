import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paramReplace'
})
export class ParamReplacePipe implements PipeTransform {

  transform(value: string, ...args: any[]) {
    let settings = args[0];
    if (settings != null && settings.params != null) {
      var params = settings.params;
      for (var i = 0; i < params.length; i++) {
        value = value.replace(":" + params[i][0], eval(params[i][1]));
      }
    }
    return value;
  }
}
