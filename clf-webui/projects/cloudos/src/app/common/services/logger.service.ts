import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  showToasts: boolean = true;
  constructor() {}
  log(message: string, data?: any, title?: string) {
    if (this.showToasts){
      // this.toastr.error(message, title);
    }
    console.log('Error: ' + message, data);
  }
  error(message: string, data?: any, title?: string) {
    if (this.showToasts){
      // this.toastr.error(message, title);
    }
    console.error('Error: ' + message, data);
  }
  stickyError(message: string, data?: any, title?: string) {
    if (this.showToasts) {
      // this.toastr.error(message, title, {closeButton: true, timeOut: 0});
    }
    console.error('Error: ' + message, data);
  }
  info(message: string, data?: any, title?: string) {
    if (this.showToasts) {
      // this.toastr.info(message, title);
    }
    console.info('Info: ' + message, data);
  }
  stickyInfo(message: string, data?: any, title?: string) {
    if (this.showToasts) {
      // this.toastr.info(message, title, {closeButton: true, timeOut: 0});
    }
    console.info('Info: ' + message, data);
  }
  stickyWarning(message: string, data?: any, title?: string, onShown?: any, onHidden?: any, closeOnHover?: any) {
    if (this.showToasts) {
        /* this.toastr.warning(message, title, {
        closeButton: false,
        timeOut: 0,
        closeOnHover: closeOnHover,
        onShown: onShown,
        onHidden: onHidden
      }); */
    }
    console.warn('Info: ' + message, data);
  }
  success(message: string, data?: any, title?: string) {
    if (this.showToasts){
      // this.toastr.success(message, title);
    }
    console.info('Success: ' + message, data);
  }
  warning(message: string, data?: any, title?: string) {
    if (this.showToasts) {
      // this.toastr.warning(message, title);
    }
    console.warn('Warning: ' + message, data);
  }
  clear() {
    console.clear();
    // this.toastr.clear();
  }
}
