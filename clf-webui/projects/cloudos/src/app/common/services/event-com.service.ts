import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventComService {
  private translationsLoaded:boolean;
  private themesLoaded:boolean;
  private settingsLoaded:boolean;
  logoutSuccess: boolean;
  SEARCH_MODE: string;
  CONTENT_MODE: string;
  shellMode: Subject<string>;
  showBars: Subject<void>;// 'show_bars' rootScope Event
  closeAllPopup : Subject<void>; //'close-all-popup' rootScope Event
  changeLogoutText : Subject<void>;// 'changeLogoutText' rootScope Event
  resetSearchFilters : Subject<void>;// 'reset-search-filters' rootScope Event
  settingsChanged: Subject<void>;// 'settingsChanged' rootScope Event
  forceLogout: Subject<string>;// 'force-logout' rootScope Event
  openLeftNav: Subject<void>;// 'open_left_nav' scope Event
  closeLeftNav: Subject<void>;// 'close_left_nav' scope Event
  offLoader: Subject<void>;// 'offLoader' scope Event
  onLoader: Subject<any>;// 'onLoader' scope Event
  exitSearch: Subject<void>; // 'exitSearch' scope event
  appDependenciesResolved : Subject<void>;// 'appDependenciesResolved' rootScope Event
  appSettingsLoaded : Subject<void>; // 'appSettingsLoaded' rootScope Event
  clUiSearchStart: Subject<string>;// 'cl.ui.search.start' scope event
  clUiSearchReStart: Subject<string>;// 'cl.ui.search.reStart' rootScope Event
  clUiSearchClear: Subject<void>;// 'cl.ui.search.clear' scope event
  logClicked: Subject<void>;// 'logo-clicked' rootScope Event
  resetMonitorMap: Subject<void>;//'reset-monitor-map' rootScope event
  bclabel: Subject<string>; // 'bclabel' rootScope Variable;
  constructor() {
    this.translationsLoaded = false;
    this.themesLoaded = false;
    this.settingsLoaded = false;
    this.logoutSuccess = false;
    this.SEARCH_MODE = 'search_mode';
    this.CONTENT_MODE = 'content_mode';
    this.showBars = new Subject<void>();
    this.closeAllPopup = new Subject<void>();
    this.changeLogoutText = new Subject<void>();
    this.resetSearchFilters = new Subject<void>();
    this.settingsChanged = new Subject<void>();
    this.forceLogout = new Subject<string>();
    this.openLeftNav = new Subject<void>();
    this.closeLeftNav = new Subject<void>();
    this.offLoader = new Subject<void>();
    this.onLoader = new Subject<any>();
    this.exitSearch = new Subject<void>();
    this.appDependenciesResolved = new Subject<void>();
    this.appSettingsLoaded = new Subject<void>();
    this.clUiSearchStart = new Subject<string>();
    this.clUiSearchReStart = new Subject<string>();
    this.clUiSearchClear = new Subject<void>();
    this.logClicked = new Subject<void>();
    this.resetMonitorMap = new Subject<void>();
    this.bclabel = new Subject<string>();
    this.shellMode = new Subject<string>();
   }
  getTranslationsLoadStatus() {
    return this.translationsLoaded;
  };

  setTranslationsLoadStatus(status:boolean) {
    this.translationsLoaded = status;
  };

  getThemeLoadStatus() {
    return this.themesLoaded;
  };

  setThemeLoadStatus(status:boolean) {
    this.themesLoaded = status;
  };

  getSettingsLoadStatus() {
    return this.settingsLoaded;
  };

  setSettingsLoadStatus(status:boolean) {
    this.settingsLoaded = status;
  };
}
