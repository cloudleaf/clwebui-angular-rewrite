import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import * as lodash from 'lodash'
import { EventComService } from './common/services/event-com.service';
import { DefaultsService } from './defaults.service';
import { PropertyService } from './property/property.service';
import { UserService } from './user/user.service';
import { environment } from '../environments/environment';
import { setTheme } from 'ngx-bootstrap/utils';
import { PermissionsHandlerService } from './auth/permissions-handler.service';
import { LoggerService } from './common/services/logger.service';
import { CustomRoute } from '../app/custom-route.type';

@Component({
  selector: 'shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.css']
})
export class ShellComponent implements AfterViewInit{
  private SEARCH_MODE: string;
  CONTENT_MODE: string;
  SHELL_MODE : string;
  isAppReady: boolean = false;
  isLeftNavBar: boolean = false;
  isLoading: boolean = true;
  headless: boolean = false;
  isBootstrapDone: boolean;
  private isConfigLoadedChecker:any = null;
  private first_show: boolean = true;
  private sidebar: boolean = false;
  private error: boolean = false;
  private user: any = null;
  private stickyToastrShown: boolean = false;
  showSplash: boolean = false;
  isBusy: boolean = false;
  busyMessage: string = 'Please wait ...';
  title: any;
  sessionTimeOutValue: any = null;
  count: number = 0;
  keyDebounce: any;
  mouseMoveDebounce: any;
  defaultRoute: CustomRoute;
  routeLoaded: string;

  constructor(private http: HttpClient
    , private router: Router
    , private eventComService: EventComService
    , private userService : UserService
    , private propertyService: PropertyService
    , private defaultsService: DefaultsService
    , private logger: LoggerService
    , private permissionsHandler: PermissionsHandlerService){
      this.isBootstrapDone = false;
      this.routeLoaded = window.location.hash;
      this.defaultRoute = {config: {settings: {rank : 0}}};
      this.SEARCH_MODE = this.eventComService.SEARCH_MODE;
      this.CONTENT_MODE = this.eventComService.CONTENT_MODE;
      this.SHELL_MODE = this.CONTENT_MODE;
      setTheme('bs3');
  }
  getApplicationContext(){
    return this.http.get('api/1/module/indoor').toPromise();
  }
  initScroll(){
    let $mainScroll:any,
        $subNav:any, tint,
        newScroll, newTop;
    setTimeout(() => {
      $mainScroll = $('.main-scroll');
      $subNav = $('#secondary-nav-container');
      $mainScroll.scroll(() => {
        newScroll = $mainScroll.scrollTop();
        newTop = -(newScroll / 3);
        // $subNav.css('top', newTop + 'px');
        $subNav.css('transform', 'translateY(' + newTop + 'px)');
        tint = 248 - (newScroll / 6);
        $subNav.css('background', 'rgb(' + tint + ',' + tint + ',' + tint + ')');
      });
    }, 300);
  }
  closeAllPopups() {
    this.eventComService.closeAllPopup.next();
  }
  getDefaultRoute(){
    let routes: any[] = this.router.config;
    let filteredRoutes: any[] = routes.filter((route: CustomRoute) => {
      return route.config.settings.isPrimary && this.permissionsHandler.isValidRoute('/'+ route.path);
    })
    filteredRoutes = filteredRoutes.sort((r1: CustomRoute, r2: CustomRoute) => {
      return r1.config.settings.rank - r2.config.settings.rank;
    });
    this.defaultRoute = filteredRoutes[0];
    if(lodash.isEmpty(this.routeLoaded)){
      if (this.userService.getUser().type === 'LIMITED_ACCESS_USER') {
        this.router.navigateByUrl('/user/myprofile');
      } else {
        if(lodash.isEmpty(this.defaultRoute)){
          this.router.navigateByUrl(this.defaultsService.getDefaultRoute());
        }else{
          this.router.navigateByUrl('/'+ this.defaultRoute.path);
        }
      }
    }else{
        let url = decodeURIComponent(this.routeLoaded).split('?');
        let state = url[0].split('#')[1];
        if(lodash.isEmpty(url[1])){
          this.router.navigate([state]);
        }else{
          let queryParms = JSON.parse('{"' + url[1].replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === '' ? value : decodeURIComponent(value)
          });
          this.router.navigate([state, ...queryParms]);
        }
    }
  }
  handleAllEvents(){
    /* $rootScope.$on('$locationChangeStart', function (obj, newloc, oldloca) {
      // off loader during state change
      $scope.isLoading = true;

      if (!navigator.onLine) {
        logger.info('It seems that you're offline. Try again when you have connectivity');
        return false;
      }

      if (newloc.match('asset_advanced_detail')) {
        exitSearch();
      }
    });

    $rootScope.$on('$locationChangeSuccess', function () {
      $scope.isLoading = false;
    });
    $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
      AnalyticsService.sendPageLoadedTiming(next);
    });
    $scope.$on('$routeChangeStart', function (event, next, current) {
      if (next && next.$$route) {
        _validateRouteChange(event, next.$$route.originalPath, '', current);
        AnalyticsService.sendStateLoad(next.originalPath);
        next.clTimestamp = new Date().getTime();
        AnalyticsService.resetRequestCounter();
      }
    });
    */
   this.eventComService.appDependenciesResolved.subscribe(()=>{
     this.isAppReady = true;
   });
   this.eventComService.exitSearch.subscribe(() => {
     this.exitSearch();
   });
   this.eventComService.onLoader.subscribe((data: any) => {
      this.isLoading = !lodash.isEmpty(data) ? true : false;
   });
   this.eventComService.offLoader.subscribe(() => {
     this.isLoading = false;
   });
   this.eventComService.closeLeftNav.subscribe(() => {
      this.isLeftNavBar = false;
      this.eventComService.showBars.next();
   });
    this.eventComService.openLeftNav.subscribe(() => {
      this.isLeftNavBar = true;
    });
    this.eventComService.forceLogout.subscribe((args) => {
      this.forceLogout(args);
    });
    this.eventComService.settingsChanged.subscribe(() => {
      this.autoLogoutOnSession();
    })
    window.addEventListener('storage', (event) => {
      if (event.key == 'logout-triggered') {
        this.logger.info('You are being loggedout due to inactivity in another tab of this application');
        this.forceLogout();
      }
    });
  }
  closeLeftNav() {
    this.isLeftNavBar = false;
    this.eventComService.showBars.next();
  }
  exitSearch() {
    this.eventComService.shellMode.next(this.CONTENT_MODE);
    this.SHELL_MODE = this.CONTENT_MODE;
    // $rootScope.bclabel = $rootScope.oldbclabel;
    $('.search-parameter-input').val('');
    this.eventComService.resetSearchFilters.next();
  }
  showAdminNav(){
    if (this.user == null) {
      this.user = this.userService.getUser();
    }
    return (this.userService.hasPermission('TENANT_SUMMARY') || this.user.proxyLogin);
  }
  forceLogout(param?: any) {
    if (!this.eventComService.logoutSuccess) {
      this.eventComService.logoutSuccess = true;
        setTimeout(() => {
          if (!lodash.isEmpty(param) && param == 'tokenRemoved') {
            window.location.href = 'login';
          } else {
            window.location.href = 'logout';
          }
        }, 100);
    }
  }
  verifyUserUpdate() {
    if (this.userService.getUser() && this.userService.getUser().modifiedTime) {
      this.userService.userUpdate().then((res: any) => {
        if (res && res.response_code === 200) {
          setTimeout(() => {
            if (!this.stickyToastrShown) {
              this.stickyToastrShown = true;
              this.logger.stickyInfo('User details are updated.Please login again.');
            }
          }, 2000);
        }
      }).catch( (err) => {
        if (err && err.status === 409) {
          setTimeout(() => {
            if (!this.stickyToastrShown) {
              this.stickyToastrShown = true;
              this.logger.stickyInfo(err.data['error-message']);
            }
          }, 2000);
        }
      });
    }
  }
  startTimeOut() {
    this.count++;
    if (this.count === this.sessionTimeOutValue) {
      this.eventComService.changeLogoutText.next();
      this.forceLogout();
      window.localStorage.setItem('logout-triggered', 'logout');
    }
  }
  resetCount() {
    this.count = 0;
  }
  initUserSurveilance() {
    this.mouseMoveDebounce = lodash.debounce(this.resetCount, 1000);
    this.keyDebounce = lodash.debounce(this.resetCount, 1000);
  }
  autoLogoutOnSession() {
    this.sessionTimeOutValue = parseInt(this.propertyService.lookup('ui.sessionTimeout', 0));
    if (this.sessionTimeOutValue && parseInt(this.sessionTimeOutValue)) {
      this.initUserSurveilance();
      document.addEventListener('click', this.resetCount);
      document.addEventListener('mousemove', this.mouseMoveDebounce);
      document.addEventListener('keypress', this.keyDebounce);
      setInterval(this.startTimeOut, 1000 * 60);
    }
  }
  showNotification($message: any) {
    // Let's check if the browser supports notifications
    if (!('Notification' in window)) {
      console.log($message);
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === 'granted') {
      // If it's okay let's create a notification
      var notification = new Notification($message);
    }

    //  Otherwise, we need to ask the user for permission. Let's not ask anymore, if he has denied notifications for our application
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission((permission) => {
        // If the user accepts, let's create a notification
        if (permission === 'granted') {
          var notification = new Notification($message);
        }
      });
    }
  }
  initialize(){
    this.title = environment.appTitle;
    this.isBusy = true;
    this.showSplash = true;
    this.eventComService.logoutSuccess = false;
    window.localStorage.removeItem('logout-triggered');
    this.getDefaultRoute();
    this.initScroll();
    this.isAppReady = false;
    this.isConfigLoadedChecker = setInterval(() => {
      this.isAppReady = true;
      clearInterval(this.isConfigLoadedChecker);
      if(this.eventComService.getTranslationsLoadStatus() && this.eventComService.getThemeLoadStatus()) {
        this.isAppReady = true;
        clearInterval(this.isConfigLoadedChecker);
      }
    }, 400);
    setInterval(this.verifyUserUpdate, 60 * 5 * 1000);
    this.autoLogoutOnSession();
  }
  ngAfterViewInit(): void {
    Promise.all([this.propertyService.getPropertiesData(), this.getApplicationContext(),this.userService.getUserData()])
    .then((responses) => {
      let moduleResponse: any = responses[1];
      this.defaultsService.configureDefaults(moduleResponse.module);
      this.userService.setUser(responses[2]);
      this.propertyService.setProperties(responses[0]);
      let user = this.userService.getUser();
      this.permissionsHandler.validateAllRoutes(user.policies, user.uiPages);
      this.isBootstrapDone = true;
      this.initialize();
    });
  }
}
