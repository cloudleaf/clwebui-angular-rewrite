import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PropertyManagerService } from '../../property/property-manager.service';
import { EventComService } from '../../common/services/event-com.service';
import { ClUtilsService } from '../../common/services/cl-utils.service';

@Component({
  selector: 'dynamic-theme-css',
  templateUrl: './dynamic-theme-css.component.html'
})
export class DynamicThemeCssComponent implements OnInit {
  @Input() loadNowThemes: boolean = false;
  @Input() initializeOnEvent: boolean = false;
  brandingCSS: any;
  constructor(private sanitizer: DomSanitizer
    , private eventComService: EventComService
    , private clUtils: ClUtilsService
    , private propertyManagerService: PropertyManagerService) {
    this.brandingCSS = null;
  }
  private loadThemeProperties() {
    this.propertyManagerService.getThemeProperties('tenant').then((data: any) => {
      this.formatForCSS(data);
      this.eventComService.setThemeLoadStatus(true);
      if (data && data.background) {
        if (data.background.hasOwnProperty('gradient-header') || data.background.hasOwnProperty('export-report-header-bg')) {
          /* TODO:: new property 'report-header-bg' needs to be added for theming,
              which will control the header bg requirement for email templates and report header,
              for avoiding the cases with gradient-header value being a linear-gradient
              Below line is just a temporary hard-coding to cover CSafe, till then!
          */
          this.clUtils.brandingMainPropsForReports.headerBG = data.background['export-report-header-bg'] ? data.background['export-report-header-bg'] : null;
          //clUtils.brandingMainPropsForReports.headerBG = ( data.background['gradient-header'] ? '#002D74' : null );
        }
      }
    });
  }

  formatForCSS(data: any) {
    this.brandingCSS = '';
    let styleString = '', bgKey = '';
    if(data.background) {
      let bgString = '';
      for(bgKey in data.background) {
        if(data.background.hasOwnProperty(bgKey)) {
          if(bgString.length) {
            bgString += ' ';
          }
          if(bgKey === 'checkboxBG') {
            bgString += ('.cl-form-checkbox-container input[type=\'checkbox\'].cl-form-checkbox:checked ~ .cl-form-checkbox-selection { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.wt__checkbox input:checked ~ .checkmark { background: ' + data.background[bgKey] + ' !important; background-color: ' + data.background[bgKey] + ' !important;}');
            bgString += ('.cust-grid .ui-grid-selection-row-header-buttons.ui-grid-row-selected, .cust-grid .ui-grid-selection-row-header-buttons.ui-grid-all-selected {background-color: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.cl-form-radio-container input[type=\'radio\'].cl-form-radio:checked ~ .cl-form-checkbox-selection { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.wt__checkbox input:checked ~ .cl-form-radio-selection { background: ' + data.background[bgKey] + ' !important; background-color: ' + data.background[bgKey] + ' !important;}');
            bgString += ('.cl-form-radio-container:hover input[type="radio"].cl-form-radio ~ .cl-form-radio-selection { background: ' + data.background[bgKey] + ' !important; background-color: ' + data.background[bgKey] + ' !important;}');
          } else if (bgKey === 'form-action-color') {
            bgString += ('.ui-select-bootstrap .ui-select-toggle > .caret { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.' + bgKey + ' { background: ' + data.background[bgKey] + ' !important; }');
          } else if (bgKey === 'primary-action-color') {
            bgString += ('.tooltip > .tooltip-inner { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.' + bgKey + ' { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.first-col-select-grid .ui-grid-cell-contents.info-selected { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.activity .metrics-menu .activity-metrics-active, .activity .metrics-menu div:hover { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.activity-active { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.intransit .wt__asset_grid .ui-grid-row-selected .ui-grid-cell-contents { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.wt__asset_grid .ui-grid-row-selected .ui-grid-cell-contents.has-position, .wt__asset_grid .ui-grid-cell-contents.has-position:hover{background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('.activity-metrics-active:before {border-left-color: '+ data.background[bgKey] +'}');
          } else if (bgKey === 'route-background-color') {
            bgString += ('.' + bgKey + ' { background: ' + data.background[bgKey] + ' !important; }');
            bgString += ('#cl-shipment-report .loc-icon:before, #cl-shipment-report .routeTransport.travel-mode-indicator .icon { background: ' + data.background[bgKey] + ' !important; }');
          } else {
            bgString += ('.' + bgKey + ' { background: ' + data.background[bgKey] + ' !important; }');
          }
        }
      }
      styleString = bgString;
    }
    if(data.color) {
      var colorString = ' ';
      for(var cKey in data.color) {
        if(data.color.hasOwnProperty(cKey)) {
          if(colorString.length) {
            colorString += ' ';
          }
          if(cKey === 'anchorLinks') {
            colorString += ('.fa.link, a { color: ' + data.color[cKey] + ' !important; }');
            colorString += ('.wt__loader i { color: ' + data.color[cKey] + ' !important; }');
            colorString += ('.btn-link { color: ' + data.color[cKey] + ' !important; }');
            colorString += ('.locat .ui-grid-canvas .ui-grid-row p { color: ' + data.color[cKey] + '; }');
            colorString += ('.wt__asset_grid .has-position {color: ' + data.color[bgKey] + ' !important; }');
          } else {
            colorString += ('.' + cKey + ' { color: ' + data.color[cKey] + ' !important; }');
          }
        }
      }
      styleString = styleString.length ? (styleString + ' ' + colorString) : colorString;
    }
    if(data.bgNColor) {
      var bNCString = ' ', splitVal = ' ';
      for(var bKey in data.bgNColor) {
        if(data.bgNColor.hasOwnProperty(bKey)) {
          splitVal = data.bgNColor[bKey].split(';');
          if (bKey === 'siteMarker') {
            bNCString += ('wt__map .site-cluster div { background: '+ splitVal[0] + '; color: ' + splitVal[1] + ' !important; }');
          } else {
            bNCString += ('.' + bKey + ' { background: '+ splitVal[0] + '; color: ' + splitVal[1] + ' !important; }');
          }
        }
      }
      styleString = styleString.length ? (styleString + ' ' + bNCString) : bNCString;
    }
    if (data.customCSS) {
      var customStr = data.customCSS.default;
      customStr = customStr && customStr.replace(/\n/g, '');
      styleString = styleString.length ? (styleString + ' ' + customStr) : customStr;
    }

    this.brandingCSS = this.sanitizer.bypassSecurityTrustHtml(styleString);
  }
  ngOnInit(): void {
    if(!this.initializeOnEvent || this.loadNowThemes == true){
      this.loadThemeProperties();
    }
  }
}

