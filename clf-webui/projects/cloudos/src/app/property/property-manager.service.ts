import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DefaultsService } from '../defaults.service';
import { UserService } from '../user/user.service';
import { environment } from '../../environments/environment';
import { Defaults } from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class PropertyManagerService {

  constructor(private httpClient: HttpClient, private defaultsService: DefaultsService, private userService: UserService) {}

  private formatForJSON(iProps:any) {
    let oProps:any = {},
      _tmp, _sTmp;
    if(iProps){
      for(var i=0, len = iProps.length; i< len; i++ ) {
        _tmp = iProps[i];
        _sTmp = _tmp.key.split('.');
        if(!oProps.hasOwnProperty(_sTmp[0]) ) {
          oProps[_sTmp[0]] = {};
        }
        if(_sTmp[1]) {
          oProps[_sTmp[0]][_sTmp[1]] = _tmp.value;
        } else {
          oProps[_sTmp[0]] = _tmp.value;
        }
      }
    }
    return oProps;
  }
  getProperties( params: any, pathParams: any, dontFormat: any ){
    return new Promise((resolve, reject)=>{
      let url = 'api/1/property/bulk/' + pathParams.prefix1 + '/' + pathParams.prefix2 ;
      if(pathParams.locale)
      url = url + "?domainName=" + pathParams.domainName + "&locale=" + pathParams.locale;
      if(pathParams.tenantId){
        url = url + "&tenantId=" + pathParams.tenantId;
      }
      this.httpClient.get(url, { params: params }).toPromise()
        .then((response:any) => {
          let data = response.data;
          if (typeof data !== "undefined") {
            if(dontFormat){
              resolve(data);
            } else {
              resolve(this.formatForJSON(data));
            }

          } else {
            resolve({});
          }
        })
        .catch(() => {
        reject();
        console.error("Something went wrong while fetching properties from Property manager!");
      });
    });
  }
  putPropeties( params:any ){
    return new Promise((resolve, reject)=>{
      var url = 'api/1/property/bulk';
      this.httpClient.put(url,  params).toPromise()
        .then((response:any) => {
          var data = response.data;
          if (typeof data !== "undefined") {
            resolve(data);
          } else {
            resolve({});
          }
        }).catch(() => {
        reject();
        console.error("Something went wrong while fetching properties from Property manager!");
      });
    });
  }
  deleteProperties(property: any){
    return new Promise((resolve, reject) => {
      let url = 'api/1/property/'+ property.prefix1 +'/'+ property.prefix2 +'/'+ property.key + '?locale='+property.locale;
      if(property.tenantId){
        url = url + "&tenantId=" + property.tenantId;
      }
      if(property.domainName){
        url = url + "&domainName=" + property.domainName;
      }
      this.httpClient.delete(url, {}).toPromise()
        .then((response : any) => {
          var data = response.data;
          if (typeof data !== "undefined") {
            resolve(data);
          } else {
            resolve({});
          }
        }).catch(() => {
        reject();
        console.error("Something went wrong while fetching properties from Property manager!");
      });
    });
  }
  getTranslationProperties(appPrefix: string, domainOnly: string, tenantId: string, dontformat: boolean){
    let params: any = {
        domainName: (window.location.hostname).split(environment.hostName)[0],
        locale: this.userService.getUser().locale
      },
      pathParams = {
        prefix2: 'tenant.labels',
        prefix1: appPrefix || this.defaultsService.getAppContext()
      };
    if(!domainOnly) {
      params.tenantId = tenantId || this.userService.getUser().effectiveTenantId;
    }
    if(params.domainName === Defaults.localhost) {
      params.domainName = environment.domain;
    }

    return this.getProperties(params, pathParams, dontformat);
  };
  getThemeProperties(level: string, forApp?: string, tenantId?: string, dontFormat?: boolean){
    let _level = level || 'domain',
      params:any = {
        domainName: (window.location.hostname).split(environment.hostName)[0],
        locale: 'NA'
      },
      pathParams = {
        prefix1: 'appCommon',
        prefix2: 'appCSS'
      };

    if(params.domainName === Defaults.localhost) { //for local testing.
      params.domainName = environment.domain;
    }

    if(_level !== 'domain') {
      params.tenantId = tenantId || this.userService.getUser().tenantId;
    }
    if (forApp) {
      pathParams.prefix1 = this.defaultsService.getAppContext();
    }

    return this.getProperties(params, pathParams,dontFormat);
  };
  putThemeProperties(params: any){
    return this.putPropeties(params);
  };
  getAppProperties(pathParams: any, level: string, tenantId: string){
    var params: any = {
        domainName: (window.location.hostname).split(environment.hostName)[0],
        locale: 'NA'
      };
    if(params.domainName === Defaults.localhost) { //for local testing.
      params.domainName = environment.domain;
    }
    if(level !== 'domain') {
      params.tenantId = tenantId || this.userService.getUser().effectiveTenantId;
    }
    return this.getProperties(params, pathParams,true);
  };
  getAppSettingProperties(appPrefix: any) {
    var payload = {
      domainName: (window.location.hostname).split(environment.hostName)[0],
      locale: 'NA',
      tenantId: this.userService.getUser().effectiveTenantId
    },
    pathParams = {
      prefix1: 'common',
      prefix2: 'appSettings'
    };

    if(payload.domainName === 'localhost') {
      payload.domainName = environment.domain; //for local testing.
    }

    if (appPrefix) {
      pathParams.prefix1 = this.defaultsService.getAppContext();
    }

    return this.getProperties(payload, pathParams, false);
  }
}
