import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {
  properties: any;
  defaultDateFormat: string = 'MM/DD/YYYY h:mm:ss a';
  constructor(private http: HttpClient) { }
  setProperties(properties: any){
    this.properties = properties;
  }
  getProperties() {
    return this.properties;
  }
  getPropertiesData(){
    return this.http.get('api/1/user/properties?inclTenant=true').toPromise();
  }
  lookup(label:string, defaultVal:any) {
    let rv = this.properties[label];
    if (rv == null){
      rv = defaultVal;
    }
    return rv;
  };

  setOrAddUserProperty (attr: any, val: any) {
    /*UserPropertyFactory.get({}, function (response) {
      var properties = response;
      if (properties[attr] != val) {
        properties[attr] = val;

        UserPropertyFactory.save(properties, function (response2) {
          UserPropertyFactory.get({inclTenant: true}, function (response3) {
            PROPERTIES = response3;
          });
        });

      }
    });*/
  }

}
