export enum Defaults {
  'indoorRoute' = '/assets',
  'indoorAppContext' = 'indoor',
  'intransitAppContext' = 'intransit',
  'intransitRoute' = '/shipments_dashboard',
  'safe2GoAppContext' = 'safe2go',
  'safe2GoRoute' = 'assets',
  'localhost' = 'localhost'
}
