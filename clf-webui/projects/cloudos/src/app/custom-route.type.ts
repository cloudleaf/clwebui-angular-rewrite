import { Route } from "@angular/router";

export interface CustomRoute extends Route{
  config: {
    isdefault?: boolean,
    hasBack?: boolean,
    hasHome?: boolean,
    title?: string,
    leftNav?: string,
    icon?: string,
    initSubTitle?: string,
    reloadOnSearch?: boolean,
    translateKey?: string,
    breadcrumb?: string,
    subParent?: string,
    settings: {
      rank: number,
      isPrimary?: boolean,
      secondaryNav?: string,
      bclabel?: string,
      permissions?: string[],
      content?: string,
      menuButtonClassName?: string,
      entity?: string,
      parent?: string,
      pageTitleKey?: string,
      timelineFixed?: boolean
      showFields?: string[],
      showFieldsLegend?: string[],
      hide?: boolean
    }
  }
}
