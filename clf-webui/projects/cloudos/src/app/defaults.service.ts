import { Injectable } from '@angular/core';
import { Defaults } from './defaults';

@Injectable({
  providedIn: 'root'
})
export class DefaultsService {
  private appContext: string;
  private defaultRoute: string;

  constructor() {
    this.appContext = '';
    this.defaultRoute = '';
  }
  private setAppContext(appContext: string){
    this.appContext = appContext;
  }
  getAppContext(){
    return this.appContext;
  }
  private setDefaultRoute(route: string){
    this.defaultRoute = route;
  }
  getDefaultRoute(){
    return this.defaultRoute;
  }
  isIndoor(){
    return this.appContext === Defaults.indoorAppContext;
  }
  isIntransit(){
    return this.appContext === Defaults.intransitAppContext;
  }
  isSafe2Go(){
    return this.appContext === Defaults.safe2GoAppContext;
  }
  configureDefaults(module: string){
    if(Defaults.indoorAppContext.toLowerCase() === module.toLowerCase()){
      this.setAppContext(Defaults.indoorAppContext);
      this.setDefaultRoute(Defaults.indoorRoute);
    } else if(Defaults.intransitAppContext.toLowerCase() === module.toLowerCase()){
      this.setAppContext(Defaults.intransitAppContext);
      this.setDefaultRoute(Defaults.intransitRoute);
    }else if(Defaults.safe2GoAppContext.toLowerCase() === module.toLowerCase()){
      this.setAppContext(Defaults.safe2GoAppContext);
      this.setDefaultRoute(Defaults.safe2GoRoute);
    }
  }
}
