import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerSupportService {

  constructor(private httpClient: HttpClient) { }
  getSubjectOptions(){
    return this.httpClient.get('api/1/customer-query/subjectOptions').toPromise();
  }

  submitSupportRequest(){
      return this.httpClient.get('api/1/customer-query').toPromise();
  }
}
