import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class PermissionsHandlerService {

  constructor() { }
  DEFAULT_PERMISSION: string = "ANY";

  topNavHash: any[] = [
      {
        key: 'Asset',
        route: '/assets',
        permissions: [this.DEFAULT_PERMISSION],
        defaultTabs: ['/assets_monitor', '/assets_metrics', '/assets_location_history', '/assets_inventory', '/assets_workpaths'],
        parent: ""
      },
      {
        key: 'Tenant',
        route: '/tenant_list',
        permissions: ['TENANT_SUMMARY'],
        defaultTabs: ['/tenant_list_s', '/all_tenants', '/admin_dashboard', '/tenant_share'],
        parent: ""
      },
      {
        key: 'Properties',
        route: '/properties',
        permissions: ['CUSTOM_PROPERTY_SUMMARY'],
        defaultTabs: ['/themes', '/appsettings', '/translations'],
        parent: ""
      },
      {
        key:'User',
        route: '/admin/user_admin',
        permissions: ['USER_SUMMARY', 'ROLE_SUMMARY'],
        defaultTabs: ['/admin/user_admin_s', '/admin/role_admin', '/admin/user_groups', '/admin/policy_admin', '/admin/user_policies'],
        parent: ""
      },
      // {route: '/building_list', permissions: ['SITE_SUMMARY'], defaultTabs: ['/building_list_s'], parent: ""},
      {
        key: 'Sensor',
        route: '/receiver_list',
        permissions: ['RECEIVER_SUMMARY', 'SENSORTAG_SUMMARY', 'TAGGEDASSET_SUMMARY', 'PERIPHERALMETA_SUMMARY'],
        defaultTabs: ['/operations_center', '/receiver_list_s', '/tag_list', '/settings/types', '/network_backhaul', '/cloud_infra'],
        parent: ""
      },
      {
        route: '/asset_admin_list',
        permissions: ['TAGGEDASSET_SUMMARY', 'ASSET_SUMMARY', 'CATEGORY_SUMMARY', this.DEFAULT_PERMISSION],
        defaultTabs: ['/customize/asset_states'],
        parent: ""
      },
      {
        key: 'Settings',
        route: '/settings',
        permissions: ['TENANT_DETAIL', this.DEFAULT_PERMISSION],
        defaultTabs: ['/myprofile', '/settings/tenant_s', '/settings/carrier_integration'],
        // Commented my-notification and user-notification as part of CLOUD-7075
        // defaultTabs: ['/myprofile', '/subscriptions', '/user_subscriptions', '/settings/tenant_s'],
        parent: ""
      },
      {key: 'Restapi', route: '/restapi', permissions: ['API_ACCESS'], defaultTabs: ['/restapi'], parent: ""},
      {key: 'YfAnalytics', route: '/yf_analytics', permissions: ['ANALYTICS_DETAIL', 'ANALYTICS_SUMMARY'], defaultTabs: ['/yf_analytics'], parent: "" },
      {route: '/tiles', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/tiles'], parent: ""},
      {route: '/dashbysite', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/dashbysite'], parent: ""},
      {route: '/analytics/map', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/analytics/map'], parent: ""},
      {key: 'Reports', route: '/reports', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/reports'], parent: ""},
      {key:'Rules', route: '/rules', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/rule_s', '/rule-set_s'], parent: ""},
      { route: '/alerts_notifications', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/alerts_list', '/notifications_list'], parent: "" },
      {key:'MappingAdmin', route: '/mapping_admin', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/mapping_admin_s'], parent: ""},
      {key: 'search', route: '/search', permissions: [this.DEFAULT_PERMISSION], defaultTabs: ['/search/asset'], parent: ""}
    ];
    sideNavHash: any[] = [
      {route: '/all_tenants', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/assets', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/tiles', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/dashbysite', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/analytics/map', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      { key: 'Reports_List', route: '/reports', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/reports/locationdwelltime', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/reports/statusdwelltime', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/reports/tagbatterylevel', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/reports/alertsNotifications', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {key: 'search_asset', route: '/search', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/dashbyarea', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/dashbysite', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/dashbycategory', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/analytics', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/analytics/map', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/poc/indoor', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {route: '/analytics/sensorchart', permissions: [this.DEFAULT_PERMISSION], parent: "/"},

      {key: 'Restapi_APIList', route: '/restapi', permissions: ['API_ACCESS'], parent: "/"},
      {key: 'YfAnalytics_Dashboard', route: '/yf_analytics', permissions: ['ANALYTICS_DETAIL', 'ANALYTICS_SUMMARY'], parent: "/" },
      {route: '/asset_history', permissions: ['ASSET_DETAIL'], parent: "/"},

      {key:'User_UserGroups', route: '/admin/user_groups', permissions: ['USER_SUMMARY'], parent: "/admin/user_admin"},
      {key:'User_Users', route: '/admin/user_admin_s', permissions: ['USER_SUMMARY'], parent: "/admin/user_admin"},
      {key:'User_Roles', route: '/admin/role_admin', permissions: ['ROLE_SUMMARY'], parent: "/admin/user_admin"},
      {key:'User_Policies', route: '/admin/policy_admin', permissions: ['POLICY_SUMMARY', 'POLICY_DETAIL'], parent: "/admin/user_admin"},
      {key:'User_UIProfiles', route: '/admin/user_policies', permissions: ['USER_SUMMARY'], parent: "/admin/user_admin"},

      //DEPRECATED
      {route: '/admin/apiuser_admin', permissions: ['USER_SUMMARY'], parent: "/admin/user_admin"}, //TODO:: need to validate this route permission
      {route: '/admin/user_profile', permissions: ['USER_DETAIL'], parent: "/admin/user_admin"}, //TODO:: need to validate this route permission
      {route: '/peripheral_list__deprecated', permissions: ['PERIPHERAL_SUMMARY'], parent: "/building_list"}, //TODO:: need to validate this route permission

      // {route: '/building_list_s', permissions: ['SITE_SUMMARY'], parent: "/building_list"},
      {route: '/area_list', permissions: ['AREA_SUMMARY', 'SITE_SUMMARY', 'SITE_DETAIL'], parent: "/building_list"},
      {route: '/peripheral_list', permissions: ['PERIPHERAL_SUMMARY'], parent: "/building_list"},
      {route: '/peripheral_history', permissions: ['PERIPHERAL_DETAIL'], parent: "/building_list"},
      {route: '/peripheral_state', permissions: ['PERIPHERAL_DETAIL'], parent: "/building_list"},

      /*Assets page shows both assets and tagged assets, and has to be shown
      if the user has both or any of the permissions: ASSET_SUMMARY, TAGGEDASSET_SUMMARY */

      {route: '/assets', permissions: [this.DEFAULT_PERMISSION], parent: "/"},
      {key:'Asset_AssetsMonitor', route: '/assets_monitor', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {key: 'Asset_AssetsMetrics', route: '/assets_metrics', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {key: 'Asset_AssetsLocationHistory', route: '/assets_location_history', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {key:'Asset_AssetsInventory', route: '/assets_inventory', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {route: '/assets_categories', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {route: '/assets_states', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},
      {route: '/assets_workpaths', permissions: [this.DEFAULT_PERMISSION], parent: "/assets"},

      {route: '/asset_admin_list_s', permissions: ['ASSET_SUMMARY'], parent: "/asset_admin_list"},
      {route: '/asset_admin_list_s', permissions: ['TAGGEDASSET_SUMMARY'], parent: "/asset_admin_list"},
      {key: 'Asset_AssetAdvancedDetail', route: '/asset_advanced_detail', permissions: ['ASSET_DETAIL'], parent: "/asset_admin_list"},
      {route: '/asset_detail', permissions: ['ASSET_DETAIL'], parent: "/asset_admin_list"},
      {route: '/categories', permissions: ['CATEGORY_SUMMARY'], parent: "/asset_admin_list"},
      {route: '/customize/asset_states', permissions: [this.DEFAULT_PERMISSION], parent: "/asset_admin_list"},

      {key: 'Alerts_SystemWideAlerts', route: '/alerts_list', permissions: [this.DEFAULT_PERMISSION], parent: "/alerts_notifications" },
      {key: 'Alerts_Notifications', route: '/notifications_list', permissions: [this.DEFAULT_PERMISSION], parent: "/alerts_notifications" },
      {key: 'Rules_RulesList', route: '/rule_s', permissions: ['RECIPE_SUMMARY'], parent: "/rules"},
      {key: 'Rules_RuleSetList', route: '/rule-set_s', permissions: ['RECIPE_SUMMARY'], parent: "/rules"},
      {
        key: 'Sensor_SensorDashboard',
        route: '/operations_center',
        permissions: ['RECEIVER_SUMMARY', 'SENSORTAG_SUMMARY', 'TAGGEDASSET_SUMMARY'],
        parent: "/receiver_list"
      },
      {route: '/network_backhaul', permissions: [this.DEFAULT_PERMISSION], parent: "/receiver_list"},
      {route: '/cloud_infra', permissions: [this.DEFAULT_PERMISSION], parent: "/receiver_list"},
      {key: 'Sensor_GatewayList', route: '/receiver_list_s', permissions: ['RECEIVER_SUMMARY', 'RECEIVER_DETAIL'], parent: "/receiver_list"},
      {key: 'Sensor_GatewayDetail', route: '/receiver_detail', permissions: ['RECEIVER_SUMMARY', 'RECEIVER_DETAIL'], parent: "/receiver_list"},
      {key:'Sensor_SensorList', route: '/tag_list', permissions: ['SENSORTAG_SUMMARY', 'TAGGEDASSET_SUMMARY'], parent: "/receiver_list"},
      {key: 'Sensor_SensorDetail', route: '/tag_detail', permissions: ['SENSORTAG_SUMMARY', 'SENSORTAG_DETAIL'], parent: "/receiver_list"},
      {key:'Sensor_SensorTypes', route: '/settings/types', permissions: ['PERIPHERALMETA_SUMMARY'], parent: "/receiver_list"},

      {key: 'Settings_TenantSettings', route: '/settings/tenant_s', permissions: ['TENANT_DETAIL'], parent: "/settings"},
      {key: "Settings_Carrier", route: '/settings/carrier_integration', permissions: ['CARRIER_SUMMARY'], parent: "/settings"},
      // {route: '/subscriptions', permissions: [this.DEFAULT_PERMISSION], parent: "/settings"},
      // {route: '/user_subscriptions', permissions: ['USER_SUMMARY', 'SUBSCRIPTION_SUMMARY'], parent: "/settings"},

      {key:'Settings_UserMyprofile', route: '/user/myprofile', permissions: ['USER_DETAIL'], parent: "/settings"},

      {key: 'Tenant_TenantList', route: '/tenant_list_s', permissions: ['TENANT_SUMMARY'], parent: "/settings/tenant"},
      {key: 'Tenant_TenantShareAccess', route: '/tenant_share', permissions: ['ACCESSIBLE_TENANCIES_SUMMARY'], parent: "/settings/tenant"},
      {route: '/all_tenants', permissions: ['TENANT_SUMMARY'], parent: "/settings"},
      {route: '/admin_dashboard', permissions: ['TENANT_SUMMARY'], parent: "/settings"},

      {key: 'Properties_Themes', route: '/themes', permissions: [ 'CUSTOM_PROPERTY_SUMMARY'], parent: "/properties"},
      {key: 'Properties_Appsettings', route: '/appsettings', permissions: [ 'CUSTOM_PROPERTY_SUMMARY'], parent: "/properties"},
      {key: 'Properties_translations', route: '/translations', permissions: [ 'CUSTOM_PROPERTY_SUMMARY'], parent: "/properties"},

      // Mapping admin
      {key: 'MappingAdmin_List', route: '/mapping_admin_s', permissions: [this.DEFAULT_PERMISSION], parent: "/mapping_admin"},

    ];
    _actionHash : any[] = [
        {
          route: '/',
          actions: [
            {action: "change_name", permissions: ['USER_DETAIL', 'USER_UPDATE']},
            {action: "change_pswd", permissions: ['USER_UPDATE']},
            {action: "change_security_questions", permissions: ['USER_UPDATE']}
          ]
        },
        {
          route: '/rule_s',
          actions: [
            {action: "add_business_rule", permissions: ["RECIPE_CREATE"]},
            {action: "edit_business_rule", permissions: ["RECIPE_UPDATE"]},
            {action: "delete_business_rule", permissions: ["RECIPE_DELETE"]}
          ]
        },
        {
          route: '/rule-set_s',
          actions: [
            {action: "add_business_rule_set", permissions: ["RECIPE_CREATE"]},
            {action: "edit_business_rule_set", permissions: ["RECIPE_UPDATE"]},
            {action: "delete_business_rule_set", permissions: ["RECIPE_DELETE"]},
            {action: "detail_business_rule_set", permissions: ["RECIPE_DETAIL"]},
            { action: "delete_shipment_rule_set", permissions: ["SHIPMENT_DELETE"] },
            { action: "delete_area_rule_set", permissions: ["AREA_DELETE"] },
            { action: "delete_site_rule_set", permissions: ["SITE_DELETE"] },
            { action: "delete_category_rule_set", permissions: ["CATEGORY_DELETE"] },
            { action: "add_shipment_rule_set", permissions: ["SHIPMENT_CREATE"] },
            { action: "add_area_rule_set", permissions: ["AREA_CREATE"] },
            { action: "add_site_rule_set", permissions: ["SITE_CREATE"] },
            { action: "add_category_rule_set", permissions: ["CATEGORY_CREATE"] }
          ]
        },
        {
          route: '/admin/user_groups',
          actions: [
            {action: "add_user_group", permissions: ["USER_CREATE", "ROLE_SUMMARY"]},
            {action: "edit_user_group", permissions: ["USER_UPDATE"]},
            {action: "delete_user_group", permissions: ["USER_DELETE"]}
          ]
        },
        {
          route: '/admin/user_admin_s',
          actions: [
            {action: "add_user", permissions: ["USER_CREATE", "ROLE_SUMMARY"]},
            {action: "edit_user", permissions: ["USER_UPDATE"]},
            {action: "delete_user", permissions: ["USER_DELETE"]},
            {action: "view_user", permissions: ["USER_DETAIL"]},
            {action: "bulk_add_user", permissions: ["USER_CREATE", "ROLE_SUMMARY"]},
            {action: "bulk_delete_user", permissions: ["USER_DELETE"]},
          ]
        },
        {
          route: '/admin/role_admin',
          actions: [
            {action: "add_role", permissions: ["ROLE_CREATE"]},
            {action: "edit_role", permissions: ["ROLE_UPDATE"]},
            {action: "delete_role", permissions: ["ROLE_DELETE"]}
          ]
      },
      {
        route: '/admin/policy_admin',
        actions: [
          { action: "add_policy", permissions: ["POLICY_CREATE"] },
          { action: "edit_policy", permissions: ["POLICY_UPDATE"] },
          { action: "delete_policy", permissions: ["POLICY_DELETE"] },
          { action: "view_policy", permissions: ["POLICY_DETAIL"] }
        ]
      },
      {
        route: '/admin/user_policies',
        actions: [
          {action: "add_user_policy", permissions: ["UI_PROFILE_CREATE"]},
          {action: "edit_user_policy", permissions: ["UI_PROFILE_UPDATE"]},
          {action: "delete_user_policy", permissions: ["UI_PROFILE_DELETE"]},
          {action: "view_user_policy", permissions: ["UI_PROFILE_DETAIL"]}
        ]
      },
      // {
      //     route: '/building_list_s',
      //     actions: [
      //       {action: "add_site", permissions: ["SITE_CREATE"]},
      //       {action: "bulk_add_site", permissions: ["SITE_CREATE"]},
      //       {action: "edit_site", permissions: ["SITE_UPDATE"]},
      //       {action: "delete_site", permissions: ["SITE_DELETE"]},
      //       {action: "view_site_detail", permissions: ["SITE_DETAIL", "AREA_SUMMARY"]}
      //     ]
      //   },
        {
          route: '/area_list',
          actions: [
            {action: "add_area", permissions: ["AREA_CREATE"]},
            {action: "edit_area", permissions: ["RECEIVER_DETAIL", "RECEIVER_SUMMARY", "AREA_UPDATE"]},
            {action: "delete_area", permissions: ["AREA_DELETE"]},
            {action: "link_receiver", permissions: ["RECEIVER_UPDATE"]},
            {action: "delink_receiver", permissions: ["RECEIVER_DELETE"]}
          ],
          paramsInRoute: true
        },
        {
          route: '/assets_inventory',
          actions: [
            {action: "add_asset", permissions: ["ASSET_CREATE"]},
            {action: "edit_asset", permissions: ["ASSET_UPDATE"]},
            {action: "delete_asset", permissions: ["ASSET_DELETE"]},
            {action: "link_asset", permissions: ["TAGGEDASSET_CREATE"]},
            {action: "delink_asset", permissions: ["TAGGEDASSET_DELETE"]},
            {action: "bulk_add_asset", permissions: ["ASSET_CREATE"]},
            {action: "bulk_delete_asset", permissions: ["ASSET_DELETE"]},
            {action: "bulk_link_asset", permissions: ["TAGGEDASSET_CREATE"]},
            {action: "bulk_delink_asset", permissions: ["TAGGEDASSET_DELETE"]},
            {action: "add_state", permissions: ["CATEGORY_UPDATE", "TENANT_UPDATE"]},
            {action: "edit_state", permissions: ["CATEGORY_UPDATE", "TENANT_UPDATE"]},
            {action: "delete_state", permissions: ["CATEGORY_UPDATE", "TENANT_UPDATE"]},
            {action: "add_cat", permissions: ["CATEGORY_CREATE"]},
            {action: "edit_cat", permissions: ["CATEGORY_UPDATE"]},
            {action: "delete_cat", permissions: ["CATEGORY_DELETE"]}
          ]
        },
        {
          route: '/receiver_list_s',
          actions: [
            {action: "view_cc", permissions: ["RECEIVER_DETAIL"]},
            {action: "add_cc", permissions: ["RECEIVER_CREATE"]},
            {action: "edit_cc", permissions: ["RECEIVER_UPDATE"]},
            {action: "delete_cc", permissions: ["RECEIVER_DELETE"]},
            {action: "bulk_add_cc", permissions: ["RECEIVER_CREATE"]},
            {action: "bulk_delete_cc", permissions: ["RECEIVER_DELETE"]},
            {action: "link_cc", permissions: ["RECEIVER_UPDATE"]},
            {action: "unlink_cc", permissions: ["RECEIVER_DELETE"]},
            {action: "viewMap_cc", permissions: ["RECEIVER_DETAIL"]}
          ]
        },
        {
          route: '/tag_list',
          actions: [
            {action: "view_tag", permissions: ["SENSORTAG_DETAIL"]},
            {action: "add_tag", permissions: ["PERIPHERALMETA_SUMMARY", "SENSORTAG_CREATE"]},
            {action: "edit_tag", permissions: ["SENSORTAG_UPDATE"]},
            {action: "delete_tag", permissions: ["SENSORTAG_DELETE"]},
            {action: "bulk_add_tag", permissions: ["SENSORTAG_CREATE"]},
            {action: "bulk_delete_tag", permissions: ["SENSORTAG_DELETE"]},
            {action: "link_tag", permissions: ["TAGGEDASSET_CREATE"]},
            {action: "unlink_tag", permissions: ["TAGGEDASSET_DELETE"]},
            {action: "viewMap_cc", permissions: ["SENSORTAG_DETAIL"]}
          ]
        },
        {
          route: '/settings/types',
          actions: [
            {action: "change_tagtype", permissions: ["PERIPHERALMETA_UPDATE"]}

          ]
        },
        {
          route: '/settings/carrier_integration',
          actions: [
            {action: "carrier_create", permissions: ["CARRIER_CREATE"]},
            {action: "carrier_summary", permissions: ["CARRIER_SUMMARY"]},
            {action: "carrier_delete", permissions: ["CARRIER_DELETE"]},
            {action: "carrier_update", permissions: ["CARRIER_UPDATE"]}
            ]
        },
        {
          route: '/tag_detail',
          actions: [
            {action: "led_test", permissions: ["API_ACCESS"]}
          ],
          paramsInRoute: true
        },
        {
          route: '/settings/tenant_s',
          actions: [
            {action: "reset_settings", permissions: ["TENANT_UPDATE"]},
            {action: "new_settings", permissions: ["TENANT_UPDATE"]},
            {action: "delete_tenant_property", permissions: ["TENANT_UPDATE"]},
            {action: "update_tenant_property", permissions: ["TENANT_SETTINGS"]}

          ]
        },
        {
          route: '/tenant_share',
          actions: [
            {action: "view_tenant_share", permissions: ["ACCESSIBLE_TENANCIES_SUMMARY"]},
            {action: "add_tenant_share", permissions: ["ACCESSIBLE_TENANCIES_CREATE"]},
            {action: "edit_tenant_share", permissions: ["ACCESSIBLE_TENANCIES_UPDATE"]},
            {action: "delete_tenant_share", permissions: ["ACCESSIBLE_TENANCIES_DELETE"]},
            {action: "tenant_share_detail", permissions: ["ACCESSIBLE_TENANCIES_DETAIL"]}
          ]
        },
        {
          route: '/settings/user',
          actions: [
            {action: "override_tenant_settings", permissions: ["USER_UPDATE"]},
            {action: "update_user_property", permissions: ["USER_UPDATE"]}

          ]
        },
        {
          route: '/tenant_list_s',
          actions: [
            {action: "view_tenant", permissions: ["TENANT_DETAIL"]},
            {action: "add_tenant", permissions: ["TENANT_CREATE"]},
            {action: "edit_tenant", permissions: ["TENANT_UPDATE"]},
            {action: "delete_tenant", permissions: ["TENANT_DELETE"]}

          ]
        },
        {
          route: '/asset_advanced_detail',
          actions: [
            {action: "upload_asset_image", permissions: ["ASSET_UPDATE"]},
            {action: "update_asset_image", permissions: ["ASSET_UPDATE"]},
            {action: "gen_asset_report", permissions: ["ASSET_DETAIL"]}
          ]
        },
        {
          route: '/mapping_admin_s',
          actions: [
            {action: 'site_create', permissions: ["SITE_CREATE"]},
            {action: 'site_update', permissions: ["SITE_UPDATE"]},
            {action: 'site_detail', permissions: ["SITE_DETAIL"]},
            {action: 'site_summary', permissions: ["SITE_SUMMARY"]},
            {action: 'site_delete', permissions: ["SITE_DELETE"]},

            {action: 'area_create', permissions: ["AREA_CREATE"]},
            {action: 'area_update', permissions: ["AREA_UPDATE"]},
            {action: 'area_delete', permissions: ["AREA_DELETE"]},
            {action: 'area_summary', permissions: ["AREA_SUMMARY"]},

            {action: 'route_create', permissions: ["ROUTE_CREATE"]},
            {action: 'route_update', permissions: ["ROUTE_UPDATE"]},
            {action: 'route_delete', permissions: ["ROUTE_DELETE"]},
            {action: 'route_detail', permissions: ["ROUTE_DETAIL"]}
          ]
        }
      ];
      _validSideRoutes: any[] = [];
      _validTopRoutes: any[] = [];
      _validActions: any[] = [];

    _validateFurtherPermissions(userPerms: any, routePerms: any, applyOR: any) {
      var _routePermLen = routePerms.length,
        _tmpRoutePerm, _allValid;
      if (applyOR) {
        _allValid = false;
        while (_routePermLen--) {
          _tmpRoutePerm = routePerms[_routePermLen];
          if (userPerms.indexOf(_tmpRoutePerm) > -1) {
            _allValid = true;
            break;
          }
        }
      } else {
        _allValid = true;
        while (_routePermLen--) {
          _tmpRoutePerm = routePerms[_routePermLen];
          if (userPerms.indexOf(_tmpRoutePerm) === -1) {
            _allValid = false;
            break;
          }
        }
      }
      return _allValid;
    };

    _validateActionForGivenRoute(iActionObj: any, permissions: any) {
      var _userPerms = permissions,
        _singleActionObj, _oActions = [],
        _iActions = iActionObj.actions,
        _iActionsLen = _iActions.length;

      while (_iActionsLen--) {
        _singleActionObj = _iActions[_iActionsLen];
        if (this._validateFurtherPermissions(_userPerms, _singleActionObj.permissions, false)) {
          _oActions.push(_singleActionObj.action);
        }
      }
      return _oActions;
    };

    _hasRoute(routeToCheck: any, actionObj: any) {
      let _isExit: any = false,
        _hasAlias = actionObj.hasOwnProperty('routeAlias');
      if (actionObj.paramsInRoute) {
        _isExit = (routeToCheck.indexOf(actionObj.route) > -1);
        if (!_isExit && _hasAlias) {
          _isExit = _.findIndex(actionObj.routeAlias, function (route) {
            return (routeToCheck.indexOf(route) > -1);
          });
        }
      } else {
        _isExit = (actionObj.route === routeToCheck);
        if (!_isExit && _hasAlias) {
          _isExit = _.findIndex(actionObj.routeAlias, function (route) {
            return (routeToCheck === route);
          });
        }
      }

      return (_isExit === -1) ? false : _isExit;
    };

    _validateActions(permissions: any) {
      let _vRoutes = this._validSideRoutes;
      let _vRoutesLen = _vRoutes.length;
      let _tmpVRoute: any, _tmpActionObj, _validatedAction;
      this._validActions = [];
      while (_vRoutesLen--) {
        _validatedAction = false;
        _tmpVRoute = _vRoutes[_vRoutesLen];
        _tmpActionObj = _.filter(this._actionHash, (actionObj) => {
          return this._hasRoute(_tmpVRoute, actionObj);
        });
        if (_tmpActionObj.length) {
          _validatedAction = this._validateActionForGivenRoute(_tmpActionObj[0], permissions);
        }
        if (!_.isEmpty(_validatedAction)) {
          this._validActions = this._validActions.concat(_validatedAction);
        }
      }
      /* Actions generic for application */
      _tmpActionObj = _.filter(this._actionHash, function (actionObj) {
        return (actionObj.route === '/');
      });
      _validatedAction = this._validateActionForGivenRoute(_tmpActionObj[0], permissions);
      this._validActions = this._validActions.concat(_validatedAction);

      this._validActions = _.uniq(this._validActions);
    };

    validateRoutes(routes: any, perms: any, applyOR: any) {
      var _routes = routes,
        routesLen = _routes.length,
        _singleRoute, _oRoutes = [],
        perms = _.cloneDeep(perms);
      perms.push(this.DEFAULT_PERMISSION);

      while (routesLen--) {
        _singleRoute = _routes[routesLen];
        if (this._validateFurtherPermissions(perms, _singleRoute.permissions, applyOR)) {
          _oRoutes.push(_singleRoute.route);
        }
      }
      return _.uniq(_oRoutes);
    };

    _validateTopNavTabs() {
      var len = this.topNavHash.length,
        _tmpTopNav, _validTabs = [],
        _tmpTopNavTabs, _tmpTopNavTab, _tmpTabsLen;

      while (len--) {
        _tmpTopNav = this.topNavHash[len];
        _validTabs = [];
        if (this._validTopRoutes.indexOf(_tmpTopNav.route) > -1) {
          _tmpTopNavTabs = _tmpTopNav.defaultTabs;
          _tmpTabsLen = _tmpTopNavTabs.length;
          for (var i = 0; i <= _tmpTopNavTabs.length; i++) {
            if (this._validSideRoutes.indexOf(_tmpTopNavTabs[i]) > -1) {
              _validTabs.push(_tmpTopNavTabs[i]);
            }
          }
          _tmpTopNav.validTabs = _validTabs;
        }

      }
    };

    generateValidSideRoutes(routeStr: any, routeMap: any) {
      let validRoutes: any = [];
      let filteredMap = routeMap.filter((route: any) => {
        return route.key;
      });
    if(routeStr){
      routeStr.forEach((validRoute: any) => {
        let index: number = _.findIndex(filteredMap, (tmpRoute: any) => {
          return tmpRoute.key == validRoute;
        });
        if (index > -1) {
          validRoutes.push(filteredMap[index].route);
        }
      });
    }

      return validRoutes;
    }

    generateValidTopRoutes(routeStr: any, routeMap: any) {
      var topRoutes: any[] = [];
      if(routeStr){
        var tmpRoutes = routeStr.map((tmpSideRoute: any) => {
          return tmpSideRoute.split('_')[0];
        });
        var validTopKeys = _.uniq(tmpRoutes);
        validTopKeys.forEach((validTopRoute) => {
          var index = _.findIndex(routeMap, (tmpRoute: any) => {
            return tmpRoute.key == validTopRoute;
          });
          if (index > -1) {
            topRoutes.push(routeMap[index].route);
          }
        });
      }

      return topRoutes;
    }


    isValidRoute(iRoute: any) {
      if (iRoute.indexOf(':')) {
        iRoute = iRoute.split('/:')[0];
      } else if (iRoute.indexOf('?')) {
        iRoute = iRoute.split('?')[0];
      }
      return (_.indexOf(this._validSideRoutes, iRoute) > -1);
    };

    isValidTopRoute(iRoute: any) {
      return (_.indexOf(this._validTopRoutes, iRoute) > -1);
    };

    isValidAction(iAction: any) {
      return (_.indexOf(this._validActions, iAction) > -1);
    };

    validateAllRoutes(userPermissions: any, uiProfileRoutes: any) {
      let _userPerms = userPermissions;

      this._validSideRoutes = this.generateValidSideRoutes(uiProfileRoutes, this.sideNavHash);
      this._validTopRoutes = this.generateValidTopRoutes(uiProfileRoutes, this.topNavHash);
      // _validSideRoutes = validateRoutes(sideNavHash, _userPerms);
      // _validTopRoutes = validateRoutes(topNavHash, _userPerms, true);
      this._validateTopNavTabs();

      this._validateActions(_userPerms);
    };

    validTopNavTabs(route: any) {
      return (_.find(this.topNavHash, {'route': route})).validTabs;
    };
}
