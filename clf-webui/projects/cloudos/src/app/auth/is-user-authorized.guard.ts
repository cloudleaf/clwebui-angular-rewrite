import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PermissionsHandlerService } from './permissions-handler.service';

@Injectable({
  providedIn: 'root'
})
export class IsUserAuthorizedGuard implements CanActivate {
  constructor(private permissionsHandlerService: PermissionsHandlerService){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return  this.permissionsHandlerService.isValidTopRoute(state.url)
        || this.permissionsHandlerService.isValidRoute(state.url);
  }

}
