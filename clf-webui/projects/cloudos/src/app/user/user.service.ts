import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: any;
  constructor(private http: HttpClient) { }
  public getUser(){
    return this.user;
  };
  public setUser(user: any){
    this.user = user;
  }
  getUserData(){
    return this.http.get('api/1/user').toPromise();
  }
  hasPermission(...args: string []) {
    if (this.user == null) {
      return false;
    }
    for (var i = 0; i < args.length; i++) {
      if ((this.user.policies).indexOf(args[i]) == -1)
        return false;
    }
    return true;
  };
  hasRole (...args: string[]) {
    if (this.user == null) {
      return false;
    }
    for (var i = 0; i < args.length; i++) {
      if ((this.user.roles).indexOf(args[i]) == -1)
        return false;
    }
    return true;

  };
  userUpdate(){
    return this.http.post('api/1/user/properties?inclTenant=true', {lastmodifiedtime: this.user.modifiedTime}).toPromise();
  }
}
