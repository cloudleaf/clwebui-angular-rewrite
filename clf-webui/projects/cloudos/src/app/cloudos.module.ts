import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AssetsMonitorComponent } from './assets/monitor/assets-monitor.component';
import { CloudosRoutingModule } from './cloudos-routing.module';
import { EventComService } from './common/services/event-com.service';
import { ShellComponent } from './shell.component';
import {TopNavComponent} from './layout/top-nav/top-nav.component';
import { UserService } from './user/user.service';
import { DefaultsService } from './defaults.service';
import { ClUtilsService } from './common/services/cl-utils.service';
import { CustomerSupportService } from './support/customer-support.service';
import { PropertyManagerService } from './property/property-manager.service';
import { LoggerService } from './common/services/logger.service';
import { IsUserAuthorizedGuard } from './auth/is-user-authorized.guard';
import { AssetsMetricsComponent } from './assets/metrics/assets-metrics.component';
import { LeftSideNavComponent } from './layout/left-side-nav/left-side-nav.component';
import {TranslateModule} from '@ngx-translate/core';
import { SearchBoxComponent } from './layout/top-nav/search-box/search-box.component';
import { DynamicThemeCssComponent } from './customization/dynamic-theme-css/dynamic-theme-css.component';
import { SideBarComponent } from './layout/side-bar/side-bar.component';
import { ParamReplacePipe } from './common/pipes/param-replace.pipe';
import { SuperAdminNavComponent } from './layout/super-admin-nav/super-admin-nav.component';
import { TenantDataService } from './tenant/tenant-data.service';
@NgModule({
  declarations: [ ShellComponent, AssetsMonitorComponent, TopNavComponent, AssetsMetricsComponent, SideBarComponent, LeftSideNavComponent, SuperAdminNavComponent, SearchBoxComponent, DynamicThemeCssComponent, ParamReplacePipe],
  imports: [ BrowserModule, FormsModule, HttpClientModule, CloudosRoutingModule, TranslateModule.forRoot() ],
  bootstrap: [ShellComponent],
  providers: [ EventComService , UserService , DefaultsService , ClUtilsService , PropertyManagerService , CustomerSupportService , LoggerService, IsUserAuthorizedGuard, TenantDataService ],
  exports: []
})
export class CloudOsModule {
 }
