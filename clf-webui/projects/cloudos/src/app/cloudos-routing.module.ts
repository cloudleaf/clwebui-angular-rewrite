import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { AssetsMetricsComponent } from './assets/metrics/assets-metrics.component';
import { AssetsMonitorComponent } from './assets/monitor/assets-monitor.component';
import { IsUserAuthorizedGuard } from './auth/is-user-authorized.guard';
import { CustomRoute } from './custom-route.type';


const routes: CustomRoute[] = [
  {
    path: 'assets',
    component: AssetsMonitorComponent,
    config: {
      isdefault: true,
      leftNav: 'Monitor',
      icon: 'wt__menu-icon wt__asset-icon',
      title: 'Assets',
      settings: {
        isPrimary: true,
        secondaryNav: 'Monitor',
        bclabel: 'Assets',
        rank: 11,
        content: 'Assets'
      }
    }
  },
  {
    path: 'assets_monitor',
    component: AssetsMonitorComponent,
    canActivate: [IsUserAuthorizedGuard],
    config: {
      isdefault: true,
      leftNav: 'Monitor',icon: 'wt__menu-icon wt__asset-icon',
      title: 'Assets',
      settings: {
        isPrimary: true,
        secondaryNav: 'Monitor',
        bclabel: 'Assets',
        rank: 11,
        content: 'Assets'
      }
    }
  },
  {
    path: 'assets_metrics',
    component: AssetsMetricsComponent,
    canActivate: [IsUserAuthorizedGuard],
    config: {
      title: 'Metrics',
      settings: {
        parent: 'Assets',
        content: 'Metrics',
        rank: 13
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class CloudosRoutingModule { }
