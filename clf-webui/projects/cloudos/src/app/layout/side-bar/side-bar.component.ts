import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as lodash from 'lodash';
import { PermissionsHandlerService } from '../../auth/permissions-handler.service';
import { EventComService } from '../../common/services/event-com.service';
import { CustomRoute } from '../../custom-route.type';
import { UserService } from '../../user/user.service';
@Component({
  selector: 'side-bar',
  templateUrl: './side-bar.component.html'
})
export class SideBarComponent implements OnInit {
  navRoutes: any[];
  doTranslate: boolean = false;
  sidebarbehaviour: any;
  showBars: boolean = false;
  bclabel: string = '';
  oldLabel: string = '';
  SEARCH_MODE: any;
  CONTENT_MODE: any;
  limitedAccessUser: boolean = false;
  CURRENT_USER: any;
  SHELL_MODE: string;


  constructor(private eventComService: EventComService
    , private router: Router
    , private permissionsHandler: PermissionsHandlerService
    , private userService: UserService) {
    this.navRoutes = [];
    this.CURRENT_USER = this.userService.getUser();
    this.SEARCH_MODE = this.eventComService.SEARCH_MODE;
    this.CONTENT_MODE = this.eventComService.CONTENT_MODE;
    this.SHELL_MODE = '';
  }
  exitSearch() {
    this.bclabel = this.oldLabel;
    this.SHELL_MODE = this.CONTENT_MODE;
    this.eventComService.shellMode.next(this.CONTENT_MODE);
    //TODO: FIX THIS search
    $('.search-parameter-input').val('');
    this.eventComService.resetSearchFilters.next();
  };
  openLeftNav() {
    this.showBars = this.showBars;
    if (this.showBars) {
      this.eventComService.closeLeftNav.next();
    } else {
      this.eventComService.openLeftNav.next();
    }
  }
  handleEvents(){
    this.eventComService.bclabel.subscribe((updatedBcLabel: string)=>{
      if(updatedBcLabel && (updatedBcLabel.indexOf('Search')===0)){
        this.oldLabel = lodash.clone(updatedBcLabel);
      }
      this.bclabel = updatedBcLabel;
    });
    // $rootScope.$on('$routeChangeSuccess', getNavRoutes);
    this.eventComService.shellMode.subscribe((value: string)=>{
      this.SHELL_MODE = value;
    });
    this.eventComService.showBars.subscribe(()=>{
      this.showBars = true;
    });
  }
  goBackTohome() {
    this.eventComService.resetMonitorMap.next();
    this.router.navigateByUrl('/dashboard');
  }
  getNavRoutes() {
    this.eventComService.closeLeftNav.next();
    let routes: any[] = this.router.config;
    /* this.navRoutes = routes.filter((r: CustomRoute)=>{
      return r.config.settings && r.config.settings.parent && $route.current.settings &&
        (r.config.settings.parent === $route.current.title || r.config.settings.parent === $route.current.settings.parent ||
          r.config.settings.parent === $route.current.settings.primaryNav) && (!r.config.settings.hide) &&
        this.permissionsHandler.isValidRoute(r.path);
    }).sort((r1: CustomRoute, r2: CustomRoute) => {
      return r1.config.settings.rank - r2.config.settings.rank;
    });
    this.sidebarbehaviour = this.navbarBehaviour($route.current, this.navRoutes);
    $rootScope.bclabel = this.sidebarbehaviour.label;
    this.doTranslate = this.sidebarbehaviour.translate; */
  }
  isCurrent(route: any) {
    /* if (!route.title || !$route.current) {

      if (!$route.current.title && $route.current.subParent == undefined) {
        return '';
      } else if (!$route.current.title && $route.current.subParent) {
        return 'current';
      }

    }
    var menuName = route.title;
    if ($route.current.title && $route.current.title.substr(0, menuName.length) === menuName ||
      ($route.current.settings && $route.current.settings.secondaryNav &&
        $route.current.settings.secondaryNav.substr(0, menuName.length) === menuName)) {

      return 'current';
    } else {
      return '';
    } */
  }
  navbarBehaviour(currentRoute: any, navRoutes: any[]) {
    var bcLabel = '', doTranslate = false;
    if (currentRoute.settings != null && (currentRoute.translateKey != null || currentRoute.settings.pageTitleKey != null) ) {
      bcLabel = currentRoute.settings.pageTitleKey || currentRoute.translateKey;
      doTranslate = true;
    } else if (currentRoute.settings != null && currentRoute.settings.bclabel != null) {
      bcLabel = currentRoute.settings.bclabel;
    } else if (currentRoute.settings && currentRoute.settings.parent) {
      bcLabel = currentRoute.settings.parent;
    } else {
      bcLabel = currentRoute.title;
    }
    return {
      translate: doTranslate,
      label: bcLabel,
      showBack: currentRoute.hasBack,
      showHome: currentRoute.hasHome == null || currentRoute.hasHome,
      showSubmenu: navRoutes && navRoutes.length > 1,
      breadcrumbClass: navRoutes && navRoutes.length > 0 ? 'col-md-3 col-sm-3 col-xs-7' : 'col-md-9 col-sm-9 col-xs-10'
    };
  }

  ngOnInit() {
    if (this.CURRENT_USER.type === 'LIMITED_ACCESS_USER') {
      this.limitedAccessUser = true;
    }
    this.getNavRoutes();
    this.handleEvents()
  }
}
