import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../user/user.service';
import { Location } from '@angular/common';
import * as lodash from 'lodash';
import {TenantDataService} from '../../tenant/tenant-data.service'

@Component({
  selector: 'super-admin-nav',
  templateUrl: './super-admin-nav.component.html',
  styleUrls: ['./super-admin-nav.component.css']
})
export class SuperAdminNavComponent implements OnInit {
  user: any;
  tenantName: string = '';
  @Input() tenant: string='';
  constructor(private userService: UserService
    , private location : Location
    , private tenantDataService: TenantDataService) {
  }
  getTenantName(){
    this.tenantDataService.getCustomer().then((res: any) => {
      if (!lodash.isEmpty(res) && res.hasOwnProperty('name')) {
        this.tenantName = res.name;
      }
    })
  }
  showAdminNav() {
    return (this.userService.hasPermission('SCOPE_ALL') || this.user.proxyLogin)
  };
  showReturnLink() {
    return this.user.proxyLogin
  };

  showProxyUser(){
    return (this.location.path() !== '/tenant_list_s')
  }


  ngOnInit(): void {
    this.user = this.userService.getUser();
  }

}
