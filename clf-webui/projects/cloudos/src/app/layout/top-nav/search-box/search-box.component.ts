import { Component, Input, OnInit } from '@angular/core';
import { EventComService } from '../../../common/services/event-com.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  @Input() value: string;
  constructor(private eventComService: EventComService) {
    this.value = '';
  }
  fnDoSearch(searchString: string) {
    this.eventComService.clUiSearchStart.next(searchString);
  };
  fnSearchCleared () {
    this.value = '';
    this.eventComService.clUiSearchClear.next();
  }

  ngOnInit(): void {
  }

}
