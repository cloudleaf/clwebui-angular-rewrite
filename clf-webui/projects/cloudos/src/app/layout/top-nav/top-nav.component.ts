import { Component, OnInit } from '@angular/core';
import { DefaultsService } from '../../defaults.service';
import { UserService } from '../../user/user.service';
import * as _ from 'lodash';
import { EventComService } from '../../common/services/event-com.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { PropertyService } from '../../property/property.service';
import { ClUtilsService } from '../../common/services/cl-utils.service';
import { CustomerSupportService } from '../../support/customer-support.service';
import { PropertyManagerService } from '../../property/property-manager.service';
import { CustomRoute } from '../../custom-route.type';
import { Router } from '@angular/router';
import { PermissionsHandlerService } from '../../auth/permissions-handler.service';
import { Location } from '@angular/common';

@Component({
  selector: 'top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
  routes: any[];
  customLogoUrl: any;
  poweredBy: boolean = false;
  showSearchBar: boolean = false;
  showBuildInfo: boolean = false;
  notificationOnly: boolean = false;
  subjects: any[];
  subjectLoading: boolean = false;
  searchObject: any;
  isFedaratedUser : boolean;
  isIndoor: boolean;
  isSafe2go: boolean;
  isInTransit: boolean;
  CURRENT_USER: any;
  tagLine: SafeHtml = '';
  logoURL: string = '';
  hasCustomerSupportAccess: boolean = false;
  buildLabel: boolean = false;
  showDropdown: boolean = false;
  isSuperAdmin: boolean = false;
  homeUrl: string = '';
  defaultRoute: CustomRoute;
  constructor(public loggedInUser: UserService
    , private router: Router
    , private defaultsService: DefaultsService
    , private eventComService: EventComService
    , private customerSupportService: CustomerSupportService
    , private propertyManagerService: PropertyManagerService
    , private propertyService: PropertyService
    , private sanitizer: DomSanitizer
    , private clUtils: ClUtilsService
    , private location: Location
    , private permissionsHandler: PermissionsHandlerService) {
    this.routes = this.router.config;
    this.subjects = [];
    this.searchObject = {query: null};
    this.CURRENT_USER = loggedInUser.getUser();
    this.isIndoor = this.defaultsService.isIndoor();
    this.isSafe2go = this.defaultsService.isSafe2Go();
    this.isInTransit = this.defaultsService.isIntransit();
    this.isFedaratedUser = this.CURRENT_USER.properties && this.CURRENT_USER.properties.isFederated;
    this.homeUrl = 'assets';
    this.defaultRoute = {config: {settings: {rank : 0}}};
  }
  private getNavRoutes() {
    let navRoutes = this.routes.filter((r: CustomRoute) => {
      return r.config.settings && r.config.settings.isPrimary && this.permissionsHandler.isValidTopRoute(r.path);
    }).sort( (r1: CustomRoute, r2: CustomRoute) => {
      return r1.config.settings.rank - r2.config.settings.rank;
    });
    this.defaultRoute =  navRoutes[0];
  }

  private getBuildAndDashboardStatus(){
    let _buildInfo: any = this.propertyService.lookup('app.version.number', '');
    let _dashboardEnabled: any = this.propertyService.lookup('ui.shipping_dashboard.visible', false);
    if (typeof _dashboardEnabled === 'string') {
      _dashboardEnabled = (_dashboardEnabled === 'false') ? false : (_dashboardEnabled === 'true');
    }
    if (_buildInfo && _buildInfo !== 'unknown') {
      this.showBuildInfo = true;
      this.buildLabel = _buildInfo;
    }
  }
  private handleEvents() {
   /*  $rootScope.$on('$routeChangeSuccess', function (e, o, i) {
      this.getNavRoutes();
      //So when we click on any button on the profile overlay we exit from search
      $scope.$parent.$parent.$parent.SHELL_MODE = CONTENT_MODE;
    }); */
    this.eventComService.clUiSearchStart.subscribe((searchString: string)=>{
      if (searchString && searchString.length > 0) {
        if (this.location.path().indexOf('/search') === -1) {
          this.router.navigateByUrl('/search/' + searchString);
        } else {
          this.eventComService.clUiSearchReStart.next(searchString);
        }
      }
    });
    this.eventComService.clUiSearchClear.subscribe(() => {
      this.showSearchBar = false;
      if (this.location.path().indexOf('/search') > -1) {
        this.location.back();
      }
    });
    this.eventComService.resetSearchFilters.subscribe(()=>{
      this.showSearchBar = false;
    });
    this.eventComService.exitSearch.subscribe(()=>{
      this.showSearchBar = false;
    });
    this.eventComService.closeAllPopup.subscribe(()=>{
      if ($('#logged_info_details').hasClass('in')) {
        $('#logged_info_details').addClass('hide');
      }
    });
  }
  logoClicked(){
    /* var _default = this.defaultRoute && this.defaultRoute.path;
    if ($scope.$parent.$parent.SHELL_MODE === 'search_mode') {
      $scope.$parent.$parent.SHELL_MODE = $scope.$parent.$parent.CONTENT_MODE;
      this.eventComService.exitSearch.next();
      $rootScope.bclabel = 'Assets';
      this.eventComService.resetMonitorMap.next();
      this.eventComService.resetSearchFilters.next();
      $location.path(_default);
    } else if ($location.$$path !== _default) {
      setTimeout(function () {
        $location.path(_default);
      }, 1)
    } else if ($location.$$path === _default) {
      this.eventComService.logClicked.next();
    } */
    //For the logo click
    this.eventComService.closeLeftNav.next();
  }
  topNavSelected(routeSelected: any){
    var validTopNavTabs = this.permissionsHandler.validTopNavTabs(routeSelected);
    if (validTopNavTabs[0] && validTopNavTabs[0].length) {
      // $location.path(validTopNavTabs[0]);
    }
  }
  searchClicked(searchString: any) {
    if (!this.showSearchBar) {
      this.showSearchBar = true;
      this.searchObject.query = null;
    } else {
      this.eventComService.clUiSearchStart.next(searchString);
    }
  };
  isSearchLoaded() {
    if (this.location.path().indexOf('/search/') > -1) {
      this.showSearchBar = true;
      // this.searchObject.query = $route.current.params.q;
    }
  }
  toggleLoggedInUser() {
    this.showDropdown = !this.showDropdown;
  };
  getIsSuperAdmin() {
    this.isSuperAdmin = this.loggedInUser.hasPermission('TENANT_SUMMARY');
    return this.isSuperAdmin;
  }
  isCurrent(route:any){
    /* if (!route.title || !$route.current || !$route.current.title) {
      return false;
    }
    var menuName = route.title;
    if ($route.current.title === menuName || ($route.current.settings &&
      ($route.current.settings.primaryNav === menuName ||
        $route.current.settings.parent === menuName))) {
      return true;
    } else {
      return false;
    } */
  }
  editProfile() {
    /* var modalInstance = $uibModal.open({
      templateUrl: 'user_profile.html',
      controller: 'UserProfileController',
      backdrop: 'static',
      resolve: {}
    }); */
  }
  hideSearch(){
    this.eventComService.exitSearch.next();
    this.eventComService.resetSearchFilters.next();
  }

  openPasswdChange(user: any) {
    /* var modalInstance = $uibModal.open({
      templateUrl: 'user_passwd.html',
      controller: 'UserPasswdController',
      backdrop: 'static',
      resolve: {
        user: function () {
          return user;
        },
        createOrUpdateUser: function () {
          return this.createOrUpdateUser;
        }
      }
    }); */
  }
  private getCustomerSubjectOptions() {
    if (this.loggedInUser.hasPermission('CUSTOMER_QUERY_SUMMARY') && this.loggedInUser.hasPermission('CUSTOMER_QUERY_CREATE')) {
      this.hasCustomerSupportAccess = true;
      this.subjectLoading = true;
      this.customerSupportService.getSubjectOptions().then((response: any) => {
        if (response && response.data) {
          this.subjectLoading = false;
          this.subjects = _.union(response.data);
        }
      }).catch(() => {
        this.subjectLoading = false;
        this.subjects = [];
      });
    }
  }
  private getApplicationSettings() {
    this.propertyManagerService.getAppSettingProperties(null).then((response: any) => {
      this.eventComService.setSettingsLoadStatus(true);
      var appSettings = response, reportLogo;

      if (appSettings.common && appSettings.common.logoURL) {
        this.customLogoUrl = appSettings.common.logoURL;
      }
      if (appSettings.common && appSettings.common.reportLogoUrl) {
        reportLogo = appSettings.common.reportLogoUrl;
      }
      //setting to show the powered By Cloudleaf
        // if(appSettings.common && appSettings.common.poweredBy) {
        //   this.poweredBy = (appSettings.common.poweredBy === 'true');
        // }
      if (appSettings.common && appSettings.common.tagLine) {
        this.tagLine = this.sanitizer.bypassSecurityTrustHtml(appSettings.common.tagLine);
      }
      this.logoURL = (this.customLogoUrl && this.customLogoUrl.length) ? this.customLogoUrl : './resources/svg/Cloudleaf_Logo_White.svg';
      this.poweredBy = (this.customLogoUrl && this.customLogoUrl.length && this.poweredBy);
      this.clUtils.brandingMainPropsForReports.logoUrl = (reportLogo && reportLogo.length) ? reportLogo : ((this.customLogoUrl && this.customLogoUrl.length) ? this.customLogoUrl : null);

      if (appSettings.common && appSettings.common.hideTemperature) {
        this.clUtils.appSettings.hideTempCharacteristic = (appSettings.common.hideTemperature === 'true');
      }

      if (appSettings.common && appSettings.common.shipmentsMaxCount) {
        this.clUtils.appSettings.shipmentsMaxCount = appSettings.common.shipmentsMaxCount;
      }
      this.eventComService.appSettingsLoaded.next();
    });
  }
  private getData(): void {
    if (this.CURRENT_USER.type === 'LIMITED_ACCESS_USER') {
      this.notificationOnly = true;
    }
    this.getCustomerSubjectOptions();
    this.getApplicationSettings();
  };
  ngOnInit(): void {
    this.clUtils.setLoginUserCookie();
    this.getBuildAndDashboardStatus();
    this.getNavRoutes();
    this.isSearchLoaded();
    this.getIsSuperAdmin();
    this.handleEvents();
    this.getData();
  }
}
