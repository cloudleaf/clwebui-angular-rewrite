import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionsHandlerService } from '../../auth/permissions-handler.service';
import { EventComService } from '../../common/services/event-com.service';
import { DefaultsService } from '../../defaults.service';
import { UserService } from '../../user/user.service';
import { CustomRoute } from '../../custom-route.type';

@Component({
  selector: 'app-left-side-nav',
  templateUrl: './left-side-nav.component.html',
  styleUrls: ['./left-side-nav.component.css']
})
export class LeftSideNavComponent implements OnInit {
  sideNavJson: any = {};
  navRoutes: any[];
  @Input() tenant: any;
  showBars: boolean = false;
  otherApps: any[];
  constructor(public loggedInUser: UserService
    , private router: Router
    , private defaultsService: DefaultsService
    , private eventComService: EventComService
    , private userService: UserService
    , private permissionsHandler: PermissionsHandlerService ) {
      this.navRoutes = [];
      this.otherApps = [];
    }

  handleEvents() {
    // $rootScope.$on('$routeChangeSuccess', getNavRoutes);
  }
  getNavRoutes() {
    this.sideNavJson = {Monitor: [], Manage: [], Configure: [], Developers: []};
    let routes: any[] = this.router.config;

    var filteredRoutes = routes.filter((r: CustomRoute) => {
      let settings = r.config.settings;
      return settings && settings.isPrimary && this.permissionsHandler.isValidTopRoute('/'+r.path);
    });
    filteredRoutes = filteredRoutes.sort((r1: CustomRoute, r2: CustomRoute) => {
      return r1.config.settings.rank - r2.config.settings.rank;
    });
    this.navRoutes = filteredRoutes.filter((r) => {
      if (r.title === 'Analytics') {
        return this.userService.hasRole('ROLE_APP_ANALYTICS');
      } else {
        return true;
      }
    });
  }
  isCircleIcon(nav: any) {
    return (nav.icon !== 'wt__asset-icon' && nav.icon !== 'wt__sensor-icon') ? true : false;
  };
  openLeftNav() {
    this.showBars = !this.showBars;
    if (this.showBars) {
      this.eventComService.closeLeftNav.next();
    } else {
      this.eventComService.openLeftNav.next();
    }
  };
  initScope() {
    let APPS = [
        { name: 'Indoor', url: '/cloudos/indoor', appContext: 'indoor', appRole: 'ROLE_APP_CLOUD' }
      , { name: 'Intransit', url: '/cloudos/intransit', appContext: 'intransit', appRole: 'ROLE_APP_INTRANSIT' }
      , { name: 'Safe2go', url: '/cloudos/safe2go', appContext: 'safe2go', appRole: 'ROLE_APP_SAFE2GO' }
    ], otherApps = [];

    this.showBars = true;

    this.eventComService.showBars.subscribe(()=>{
      this.showBars = true;
    });

    $('#leftNavDropdown').on('hide.bs.dropdown',  () => {
      setTimeout(() => {
        this.showBars = true;
      }, 1);
    });

    let tmpApp;
    for(let i=0, len = APPS.length; i<len; i++) {
      tmpApp = APPS[i];
      if(tmpApp.appContext !== this.defaultsService.getAppContext() && this.userService.hasRole(tmpApp.appRole)) {
        otherApps.push(tmpApp);
      }
    }
    this.otherApps = otherApps;
  }
  changeRoute (routeUrl: string) {
    this.eventComService.closeLeftNav.next();
    let validTopNavTabs = this.permissionsHandler.validTopNavTabs('/'+routeUrl);
    if (validTopNavTabs[0] && validTopNavTabs[0].length) {
      this.router.navigateByUrl(validTopNavTabs[0]);
    }
  };
  ngOnInit(): void {
    this.handleEvents();
    this.getNavRoutes();
    this.initScope();
  }
}
